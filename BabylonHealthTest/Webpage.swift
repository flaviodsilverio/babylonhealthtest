//
//  Webpage.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 02/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import UIKit

class Webpage: UIViewController, UIWebViewDelegate {

    var website : String?
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       // if website == nil {
        
            website = "http://www.google.com"
        
        //}
        
        webView.loadRequest(NSURLRequest(URL: NSURL(string: website!)!))
//        webView.delegate = self;
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        
    }
    
}
