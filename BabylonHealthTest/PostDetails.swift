//
//  PostDetails.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 01/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import UIKit

class PostDetails: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var post : Posts?
    
    //MARK: IBOutlets
    
    @IBOutlet weak var userAvatar: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postBody: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.registerNib(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        fillDetails()
        
    }
    
    func fillDetails(){
    
        userName.text = post?.user?.name
        postTitle.text = post?.title
        postBody.text = post?.body
        
        if post?.user?.avatar != nil {
        
             self.userAvatar.image = UIImage(data: (post?.user?.avatar!)!)
            
        } else {
            
            RequestManager.getAvatar(50, email: (post?.user?.email)!, callback: {
                (success, image, error) in
                if success == true {
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.userAvatar.image = image!
                        
                    })
                }
            })
        }
    }
    
    @IBAction func showUserDetails(sender: AnyObject){
    
        self.performSegueWithIdentifier("showUserDetails", sender: self)
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "showUserDetails"){
        
            let destination = segue.destinationViewController as! UserDetails
            destination.user = post?.user
        
        }
    }
    
    
    //MARK: Table view Delegate Methods
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! CustomCell
        
        let comment = post?.comments!.allObjects[indexPath.row] as! Comments
        cell.detailText.text = comment.body
        cell.username.text = comment.email
        
        cell.avatar?.image = UIImage(named: "user")
        
        if comment.avatar != nil {
            
            cell.avatar.image = UIImage(data: comment.avatar!)
            
        }
        
        RequestManager.getAvatar(50, email: (comment.email)!, callback: {
            (success, image, error) in

                var avatarImage : UIImage?
                
                if success == true {
                    
                    avatarImage = image
                    comment.avatar = UIImagePNGRepresentation(avatarImage!)
                    
                } else {
                    
                    avatarImage = UIImage(named: "user")
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    cell.avatar.image = avatarImage!
                    
                })
        })
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (post?.comments?.count)!
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Total Comments: \(post!.comments!.count)"
    }
}
