//
//  RequestManager.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 4/29/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol RequestManagerDelegate {
    
    func requestsFinished()
    func requestsProblem()
    
}

class RequestManager: AnyObject, RequestManagerDelegate {
    
    let managedObjectContext : NSManagedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var delegate = RequestManagerDelegate?()
    var basePath : String!

    
    init(){
        basePath = "http://jsonplaceholder.typicode.com/"
    }
    
    func updateData(){
        getUsers()
    }
    
    func getUsers(){
        
        webRequest("users", callBack: {
            (success: Bool?, values: [Dictionary<String, AnyObject>], error: NSError?) in
            
            if success == true {
                
                    Users.addUsers(values,
                        inContext: self.managedObjectContext,
                        withCompletion: {
                            (result: Bool?, error: NSError?) in
                            
                            if result == true {
                                
                                self.getPosts()
                                
                            } else{
                                
                                self.requestsProblem()
                                
                            }
                    })
                
            } else {
                
                self.requestsProblem()
                
            }
            
        })
    }
    
    func getComments(){
        
        webRequest("comments", callBack: {
            (success: Bool?, values: [Dictionary<String, AnyObject>], error: NSError?) in
            
            if success == true {
            
                Comments.addComments(values,
                    inContext: self.managedObjectContext,
                    withCompletion: {
                        (result: Bool?, error: NSError?) in
                        
                        if result == true {
                            
                            self.requestsFinished()
                            
                        } else{
                            
                            self.requestsProblem()
                            
                        }
                })
                
            } else {
                
                self.requestsProblem()
                
            }
        
        })

    }
    
    func getPosts(){
        
        webRequest("posts", callBack: {
            (success: Bool?, values: [Dictionary<String, AnyObject>], error: NSError?) in
            
            if success == true {
                
                Posts.addPosts(values,
                    inContext: self.managedObjectContext,
                    withCompletion: {
                        (result: Bool?, error: NSError?) in
                        
                        if result == true {
                            
                            self.getComments()
                            
                        } else{
                        
                            self.requestsProblem()

                        }
                })
                
            } else {
                
                self.requestsProblem()

            }
            
        })
    }
    
    func webRequest(endpoint: String,
                    callBack: (success: Bool?, values: [Dictionary<String, AnyObject>], error: NSError?)->()){
        
        let url: NSURL = NSURL(string: String(format: "\(basePath)\(endpoint)"))!
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {
            (data, response, error) -> Void in
            
            if error != nil {
                
                self.requestsProblem()
                
            } else {
                
                do {
                    let values = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableLeaves) as! [Dictionary<String, AnyObject>]
                    
                    callBack(success: true, values: values, error: nil)
                    
                    
                } catch {
                    self.requestsProblem()
                }
                
            }
        })
        
        task.resume()
        
    }
    
    
    class func getAvatar(size: Int, email: String?,
                         callback: (success: Bool?, image: UIImage?, error: NSError?)->()){
        
        if email != nil {
            
            
            let url: NSURL = NSURL(string: "https://api.adorable.io/avatars/\(size)/\(email!)")!
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithURL(url, completionHandler: {
                (data, response, error) -> Void in
                
                if error != nil {
                    
                    callback(success: false, image: UIImage(named: "user"), error: error)

                } else {
                    
                guard let image = UIImage(data: data!) else {
                    
                    callback(success: false, image: UIImage(named: "user"), error: error)
                    return
                    
                    }
                    
                    callback(success: true, image: image, error: nil)

                }
                
            })
            
            task.resume()
            
        } else {
        
            callback(success: false, image: UIImage(named: "user"), error: nil)
        
        }
    }
    
    //MARK: Delegate Methods
    
    func requestsFinished() {
        
        dispatch_async(dispatch_get_main_queue(), {
            self.delegate?.requestsFinished()
            
        })
        
    }
    
    func requestsProblem(){
        
        dispatch_async(dispatch_get_main_queue(), {
            self.delegate?.requestsProblem()
            
        })
    }
    
}

