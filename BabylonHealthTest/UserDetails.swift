//
//  UserDetails.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 01/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class UserDetails: UITableViewController, MKMapViewDelegate, CLLocationManagerDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet var mapView : MKMapView?
    var user : Users?
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillDetails()
    }
    
    func fillDetails(){
    
        phone.text = user?.phone
        website.text = user?.website
        name.text = user?.name
        email.text = user?.email
        
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        centerMap()
    }
    
    func centerMap(){
    
        var region = MKCoordinateRegion()
        let location = CLLocation(
            latitude: Double(user!.valueForKeyPath("address.geo.lat")! as! String)!,
            longitude: Double(user!.valueForKeyPath("address.geo.lat")! as! String)!)
        region.center = location.coordinate
        mapView?.setRegion(region, animated: true)
    
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch indexPath.section {
        
        case 0:
            
            if indexPath.row == 1 {
            
                self.performSegueWithIdentifier("showWebsite", sender: self)
            
            }
            break
            
        case 1:
            
            if indexPath.row == 0 {
                
                let mailComposeViewController = configuredMailComposeViewController()
                if MFMailComposeViewController.canSendMail() {
                    self.presentViewController(mailComposeViewController, animated: true, completion: nil)
                }
            
            } else {
                
                let alert = UIAlertController(title: "", message: "Would you like to call \(user!.phone!)", preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: {})
                })
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.Default, handler: {
                    (action) in
                    self.callNumber((self.user?.phone)!)
                })

                alert.addAction(cancelAction)
                alert.addAction(okAction)
                
                self.presentViewController(alert, animated: true, completion: {})
                
                    //UIAlertView(title: "", message: "Would you like to call \(user?.phone)", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Call")
                
            
            }
            
            break
        default:
            break
        }
    }
    
    //MARK: Phone call
    
    func callNumber(number: String){
    
        var correctedNumber = String()
        
        for character in number.characters {
        
            switch character {
            case "0","1","2","3","4","5","6","7","8","9":
                
                correctedNumber.append(character)
                
                break
                
            default:
                print("\(character) is an invalid character")
            }
        
        }
        
        let phone = "tel://" + correctedNumber
        let url = NSURL(string: phone)
        UIApplication.sharedApplication().openURL(url!)
    
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([(user?.email)!])
        
        return mailComposerVC
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
}
