//
//  Comments.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/04/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import Foundation
import CoreData


class Comments: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    class func addComments(coments: [Dictionary<String,AnyObject>],
                           inContext context: NSManagedObjectContext,
                                     withCompletion completion: (result: Bool?, error: NSError?)->()) {
        
        let localContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        localContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        localContext.parentContext = context
        
        localContext.performBlock({
            
            for coment in coments {
                
                let currentComment = createCommentWithID(coment["id"] as! NSNumber, inContext: localContext)
                
                currentComment.body = coment["body"] as? String
                currentComment.email = coment["email"] as? String
                currentComment.name = coment["name"] as? String
                
                if coment["postId"] != nil {
                    
                    currentComment.post = Posts.createPostWithID(coment["postId"] as! NSNumber, inContext: localContext)
                }
                
            }
            
            
            do {
                
                try localContext.save()
                
                completion(result: true, error: nil)
                
                
            } catch let error as NSError {
                
                print("Fetch failed: \(error.localizedDescription)")
            }
            
            
            
        })
    }
    
    
    class func createCommentWithID(postID: NSNumber, inContext managedObjectContext: NSManagedObjectContext) -> Comments {
        
        //Check if an user with said ID exists if it doesn't, create a new one
        var comment = getCommentWithID(postID, inContext: managedObjectContext)
        
        if comment == nil {
            comment = NSEntityDescription.insertNewObjectForEntityForName("Comments", inManagedObjectContext: managedObjectContext) as? Comments
            comment!.id = postID
            
            do {
                
                try managedObjectContext.save()
                
                
            } catch let error as NSError {
                print("Save failed: \(error.localizedDescription)")
                
            }
            
            
            
        }
        
        return comment!
        
    }
    
    class func getCommentWithID(commentID: NSNumber, inContext managedObjectContxt: NSManagedObjectContext) -> Comments? {
        
        let fetchRequest = NSFetchRequest(entityName: "Comments")
        let predicate = NSPredicate(format: "id == \(commentID)")
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        
        do{
            let results = try managedObjectContxt.executeFetchRequest(fetchRequest)
            
            if results.count > 0  {
                return results.first as? Comments
            }
            
        } catch let error as NSError{
            
            print("Fetch failed: \(error.localizedDescription)")
            
        }
        
        return nil
    }
    
    
    class func getAllComments(inContext managedObjectContext: NSManagedObjectContext) -> [Comments] {
        
        let fetchRequest = NSFetchRequest(entityName: "Comments")
        
        do {
            
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [Comments]
            
        } catch let error as NSError {
            
            print("Fetch failed: \(error.localizedDescription)")
            return []
            
        }
        
        
    }
    
}
