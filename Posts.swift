//
//  Posts.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/04/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class Posts: NSManagedObject {

    // Insert code here to add functionality to your managed object subclass
    
    class func addPosts(posts: [Dictionary<String,AnyObject>],
                           inContext context: NSManagedObjectContext,
                                     withCompletion completion: (result: Bool?, error: NSError?)->()) {
        
         let localContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        localContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        localContext.parentContext = context
        
        localContext.performBlock({
        
        for post in posts {
            
                let currentPost = createPostWithID(post["id"] as! NSNumber, inContext: localContext)
            
                currentPost.body = post["body"] as? String
                currentPost.title = post["title"] as? String
                
                if(post["userId"] != nil){
                    
                    currentPost.user = Users.createUserWithID((post["userId"] as! NSNumber) == 10 ? NSNumber(float:9) : post["userId"] as! NSNumber, inContext: localContext)
                    
                    if currentPost.user == nil {
                    
                    }
                }
            
            
        }
                
        
        do {
            
            try localContext.save()
        
            
            completion(result: true, error: nil)
            
            
        } catch let error as NSError {
            
            print("Fetch failed: \(error.localizedDescription)")
        }
        
        })
        
    }
    
    class func fetchFavourites(managedObjectContext: NSManagedObjectContext)->[Posts]{
    
        let fetchRequest = NSFetchRequest(entityName: "Posts")
        let predicate = NSPredicate(format: "isFavorite = \(NSNumber(bool:true))")
        
        fetchRequest.predicate = predicate
        
        do {
            
            let results = try managedObjectContext.executeFetchRequest(fetchRequest) as! [Posts]
            return results
            
        } catch let error as NSError {
            print(error)
        }
        
        return []
    }
    
    class func createPostWithID(postID: NSNumber, inContext managedObjectContext: NSManagedObjectContext) -> Posts {
        
        //Check if an user with said ID exists if it doesn't, create a new one
        var post = getPostWithID(postID, inContext: managedObjectContext)
        
        if post == nil {
            post = NSEntityDescription.insertNewObjectForEntityForName("Posts", inManagedObjectContext: managedObjectContext) as? Posts
            post!.id = postID
            managedObjectContext.insertObject(post!)
            
            do {
                
                try managedObjectContext.save()
                
            } catch let error as NSError {
                print("Save failed: \(error.localizedDescription)")
                
            }
            
            
            
        }
        
        return post!;
        
    }
    
    class func getPostWithID(postID: NSNumber, inContext managedObjectContxt: NSManagedObjectContext) -> Posts? {
        
        let fetchRequest = NSFetchRequest(entityName: "Posts")
        let predicate = NSPredicate(format: "id == \(postID)")
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1

        do{
            let results = try managedObjectContxt.executeFetchRequest(fetchRequest)
            
            if results.count > 0  {
                return results.first as? Posts
            }
            
        } catch let error as NSError{
            
            print("Fetch failed: \(error.localizedDescription)")
            
        }
        
        return nil
    }
    
    class func getAllPosts(inContext managedObjectContext: NSManagedObjectContext) -> [Posts] {
        
        let fetchRequest = NSFetchRequest(entityName: "Posts")
        
        do {
            
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [Posts]
            
        } catch let error as NSError {
            
            print("Fetch failed: \(error.localizedDescription)")
            return []
            
        }
        
        
    }
    
    func handleDidSaveNotification(notification: NSNotification){
        
        let mainContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        mainContext.mergeChangesFromContextDidSaveNotification(notification)
    
    }

}
