//
//  Posts+CoreDataProperties.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/04/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Posts {

    @NSManaged var id: NSNumber?
    @NSManaged var title: String?
    @NSManaged var body: String?
    @NSManaged var comments: NSSet?
    @NSManaged var user: Users?

}
