-----------READ ME FIRST------------

First of all, let me apologize for that previous version. It was really a minor thing that was missing since I previously didn't manage to finish it all. Now everything required has been done and I've added some bonuses:

- Made a lightweight data migration. That was so I could add a field for the avatar images and would maintain the integrity of the DB on the simulator/device the app was previously tested. That and to show that I know how to do that.

- Avatar images are now cached after their first load. This wasn't explicitly required, but calling the service each time an image was needed was completely unnecessary. This way it'll only load an image once per user/comment and cache it after that. If there's no coached image for the specific user/comment, it'll show an empty avatar placeholder.

- Added a no-data-view to the first page. That being said, if you're opening the app for the first time (or better yet, if you have no data) it'll show you firstly a loading screen and then either it'll go away or it'll tell you that something happened and give you a "retry button". The best way to try this would be launching the app without any internet connection.

Thanks for the time and for the second chance to better my own work. I'd appreciate any feedback you could give me.
Flavio