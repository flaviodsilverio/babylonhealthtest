//
//  CustomCell.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 02/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var username: UILabel!

    @IBOutlet weak var detailText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
