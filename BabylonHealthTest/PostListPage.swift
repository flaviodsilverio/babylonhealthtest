//
//  PostListPage.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 01/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import UIKit

class PostListPage: UITableViewController, RequestManagerDelegate {

    var posts : [Posts]!
    let requestManager = RequestManager()
    var remainingRequests = Int(3)
    
    
    override func viewDidLoad() {
        
        
        self.tableView.registerNib(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        requestManager.delegate = self
        posts = Posts.getAllPosts(inContext: requestManager.managedObjectContext)
        
        if posts?.count == 0 {
            
            setLoadingView(true, errorOccurred: false)
            
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        requestManager.updateData()

    }
    
    
    //MARK: Delegate Methods
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (posts?.count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! CustomCell
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

        cell.username.text = posts[indexPath.row].user!.name
        cell.detailText.text = posts[indexPath.row].title
        cell.avatar?.image = UIImage(named: "user")
        
        
        if posts[indexPath.row].user!.avatar != nil {
            
            cell.avatar.image = UIImage(data: posts[indexPath.row].user!.avatar!)

        } else {
            
            RequestManager.getAvatar(50, email: (posts[indexPath.row].user?.email)!, callback: {
                (success, image, error) in
                
                
                    var avatarImage : UIImage?
                    
                    if success == true {
                        
                        avatarImage = image
                        
                    } else {
                        
                        avatarImage = UIImage(named: "user")
                    }
                    
                    self.posts[indexPath.row].user!.avatar = UIImagePNGRepresentation(avatarImage!)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        cell.avatar.image = avatarImage!
                        
                    })
                
            })
            
        }
        return cell;
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showDetails" {
            let destination = segue.destinationViewController as! PostDetails
            destination.post = posts[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    
    //MARK: Actions
    
    @IBAction func refreshData(sender: AnyObject) {
        
        requestManager.updateData()
        
        if posts?.count == 0 {
            
            setLoadingView(true, errorOccurred: false)
            
        }
        
    }
    
    //MARK: RequestManager Delegate Methods
    
    func requestsFinished() {
        
        posts = Posts.getAllPosts(inContext: requestManager.managedObjectContext)
        self.tableView.separatorColor = UIColor.grayColor()
        setLoadingView(false, errorOccurred: false)
        self.tableView.reloadData()
    }
    
    func requestsProblem() {
        
        if posts?.count == 0 {
        
            setLoadingView(true, errorOccurred: true)
        
        } else {
            setLoadingView(false, errorOccurred: false)
        }
        
    }
    
    //MARK: Setting the view for the loading problems
    
    func setLoadingView(visible: Bool, errorOccurred: Bool) {
        
        tableView.viewWithTag(301)?.removeFromSuperview()
        
        if visible {
            
            self.tableView.separatorColor = UIColor.clearColor()
            
            let view = UIView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            view.tag = 301
            view.backgroundColor = UIColor.darkGrayColor()
            
            let label = UILabel(frame: CGRectMake(0,0,200,50))
            label.textColor = UIColor.whiteColor()
            label.center = CGPointMake(view.center.x, view.center.y - 50)
            label.textAlignment = NSTextAlignment.Center
            view.addSubview(label)
            
            if errorOccurred {

                label.text = "An Error ocurred"
                
                let button = UIButton(frame: CGRectMake(0,0,200,50))
                button.center = CGPointMake(view.center.x, view.center.y)
                button.setTitle("Tap Here to Retry", forState: UIControlState.Normal)
                button.backgroundColor = UIColor.whiteColor()
                button.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
                button.addTarget(self, action: #selector(loadingButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
                view.addSubview(button)
                
            } else {
                
                label.text = "Loading Data..."
                
                let indicator = UIActivityIndicatorView(frame: CGRectMake(0,0,100,100))
                indicator.center = view.center
                indicator.startAnimating()
                view.addSubview(indicator)

            }
            

            
            self.tableView.addSubview(view)
            
        }
    }
    
    func loadingButtonTapped(){
        
        requestManager.updateData()
        setLoadingView(true, errorOccurred: false)
    }
}
