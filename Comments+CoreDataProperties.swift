//
//  Comments+CoreDataProperties.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 04/05/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Comments {

    @NSManaged var body: String?
    @NSManaged var email: String?
    @NSManaged var id: NSNumber?
    @NSManaged var name: String?
    @NSManaged var avatar: NSData?
    @NSManaged var post: Posts?

}
