//
//  Users.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/04/16.
//  Copyright © 2016 Flavio Silverio. All rights reserved.
//

import Foundation
import CoreData


class Users: NSManagedObject {

    // Insert code here to add functionality to your managed object subclass
    
    class func addUsers(users: [Dictionary<String,AnyObject>],
                           inContext context: NSManagedObjectContext,
                                     withCompletion completion: (result: Bool?, error: NSError?)->()) {
        
        let localContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        localContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        localContext.parentContext = context
        
        localContext.performBlock({
            
        for user in users {
            
            let currentUser = createUserWithID(user["id"] as! NSNumber, inContext: localContext)
//            
            currentUser.email = user["email"] as? String
            currentUser.name = user["name"] as? String
            currentUser.phone = user["phone"] as? String
            currentUser.username = user["username"] as? String
            currentUser.website = user["website"] as? String
            currentUser.company = user["company"] as? Dictionary<String,AnyObject>
            currentUser.address = user["address"] as? Dictionary<String,AnyObject>
            
            
        }
        
        do {
            
            try localContext.save()


            completion(result: true, error: nil)
            
            
        } catch let error as NSError {
            
            print("Fetch failed: \(error.localizedDescription)")
        }
        
        
        })
        
    }
    
    class func createUserWithID(userID: NSNumber, inContext managedObjectContext: NSManagedObjectContext) -> Users {
        
        //Check if an user with said ID exists if it doesn't, create a new one
        var user = getUserWithID(userID, inContext: managedObjectContext)
        
        if user == nil {
            user = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: managedObjectContext) as? Users
            user!.id = userID

            do {
            
                try managedObjectContext.save()

            
            } catch let error as NSError {
                print("Save failed: \(error.localizedDescription)")

            }
        
            
        
        }
        
        return user!;
    
    }
    
    class func getUserWithID(userID: NSNumber, inContext managedObjectContxt: NSManagedObjectContext) -> Users? {

        let fetchRequest = NSFetchRequest(entityName: "Users")
        let predicate = NSPredicate(format: "id == \(userID)")
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        
        do{
            let results = try managedObjectContxt.executeFetchRequest(fetchRequest)
        
            if results.count > 0  {
                return results.first as? Users
            }
            
        } catch let error as NSError{
        
            print("Fetch failed: \(error.localizedDescription)")
        
        }
        
        return nil
    }
    
    class func getAllUsers(inContext managedObjectContext: NSManagedObjectContext) -> [Users] {
        
        let fetchRequest = NSFetchRequest(entityName: "Users")
        
        do {
            
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [Users]
            
        } catch let error as NSError {
            
            print("Fetch failed: \(error.localizedDescription)")
            return []
            
        }
        
        
    }

}
